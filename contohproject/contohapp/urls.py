from django.urls import include, path
from .views import index
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('', index, name='index'),
]

urlpatterns += staticfiles_urlpatterns()