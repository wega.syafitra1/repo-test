from django.shortcuts import render
from .models import Person, Post
# Create your views here.
# from django.http import HttpResponse

# def index(request):
# 	return HttpResponse ("Hello world!")

mhs_name = 'Kak PeBePe'

def index(request):
    persons = Person.objects.all().values()

    response = {'name' : mhs_name, 'persons' : persons}

    return render(request, 'index.html', response)
